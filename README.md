# lilypond-docs

### Branches

How this repository's branches are split:

- `master`: the landing branch with some info, e.g. README.md
- `translation`: exact copy of LilyPond's `translation` branch for
   better web view
- `portuguese`: sinchronized with `translation` branch, but used to
land new and fix translations
- `new/something`: temporary branch for adding new translation
- `fix/something`: temporary branch to only fix translations.

### Readings

- [Documentation translation details][LyDocs] in LilyPond documentation

- [GNU Texinfo Manual][TexinfoManual] for .texi syntax help

- [GNU Gettext Manual][GettextManual] for .po files help

### See also

- [LilyPond translations mailing list][maillist]
  ([translations@lilynet.net][email])

- ["Starting Brazilian Portuguese translation for Docs" thread][thread]

- [Localization Guide][THguide] by TranslateHouse

### Translation process

- Choose file(s) to translate

- Create a new branch `new/something` from `pt` branch, where `something` is the name of the file to be translated or other nomenclature that make sense

- Looks good? Create patch using `git format-patch` and send to the
  [translation mailing list][email]


[LyDocs]: https://lilypond.org/doc/Documentation/contributor/documentation-translation-details
[TexinfoManual]: https://www.gnu.org/software/texinfo/manual/texinfo/texinfo.html
[GettextManual]: https://www.gnu.org/software/gettext/manual/gettext.html
[maillist]: http://lilypond-translations.3384276.n2.nabble.com/
[email]: mailto:translations@lilynet.net
[thread]: http://lilypond-translations.3384276.n2.nabble.com/Starting-Brazilian-Portuguese-translation-for-Docs-td7573035.html
[THguide]: http://docs.translatehouse.org/projects/localization-guide/en/latest/guide/start.html
